package my.web.com.repository;

import my.web.com.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomizedUserRepository extends JpaRepository<User, Integer> {

    Optional<User> findByLogin(String login);

}
