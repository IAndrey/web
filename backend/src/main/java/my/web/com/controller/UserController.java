package my.web.com.controller;

import lombok.AllArgsConstructor;
import my.web.com.model.dto.RequestUserDto;
import my.web.com.model.dto.ResponseUserDto;
import my.web.com.services.UserServices;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {

    private final UserServices userServices;

    @PostMapping("/save")
    public void saveUser(@Valid @RequestBody RequestUserDto userDao) {
        userServices.saveUser(userDao);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteUser(@PathVariable("id") int id) {
        userServices.deleteUserById(id);
    }

    @GetMapping("/find/{login}")
    public ResponseUserDto findUserByNickName(@PathVariable String login){
        return userServices.findUserByNickName(login);
    }

    @GetMapping("/find/all")
    public List<ResponseUserDto> findUsers(){
        return userServices.findAllUsers();
    }

}
