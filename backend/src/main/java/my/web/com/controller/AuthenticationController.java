package my.web.com.controller;

import lombok.AllArgsConstructor;
import my.web.com.model.dto.AuthenticationRequestDto;
import my.web.com.model.dto.AuthenticationResponseDto;
import my.web.com.model.dto.ResponseUserDto;
import my.web.com.services.AuthServices;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@AllArgsConstructor
@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    private final AuthServices authServices;

    @PostMapping("/login")
    public AuthenticationResponseDto authenticate2(@RequestBody @Valid AuthenticationRequestDto request) {
        return authServices.login(request);
    }

    @GetMapping("/validate/{token}")
    public ResponseUserDto validation(@PathVariable String token) {
        return authServices.validationToken(token);
    }

    @PostMapping("/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        SecurityContextLogoutHandler securityContextLogoutHandler = new SecurityContextLogoutHandler();
        securityContextLogoutHandler.logout(request, response, null);
    }
}
