package my.web.com.model.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

@Data
public class AuthenticationRequestDto {
    @NotEmpty
    private String login;
    @NotEmpty
    @Length(min = 3, max = 100)
    private String password;
}
