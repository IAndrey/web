package my.web.com.model.dto;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ApiError {
    private final int code;
    private final String message;

    public ApiError(HttpStatus status) {
        this.code = status.value();
        this.message = status.getReasonPhrase();

    }

    public ApiError(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
