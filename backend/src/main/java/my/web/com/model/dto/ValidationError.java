package my.web.com.model.dto;

import lombok.Data;

@Data
public class ValidationError {
    private final String field;
    private final String message;
}
