package my.web.com.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import my.web.com.model.Role;
import my.web.com.model.User;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResponseUserDto {

    private int id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private int age;
    private String email;
    private int tokens;
    private Role role;

    public static ResponseUserDto convertToResponseUserDto(User user) {
        return ResponseUserDto.builder()
                .age(user.getAge())
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .login(user.getLogin())
                .password(user.getPassword())
                .role(user.getRole())
                .tokens(user.getTokens())
                .id(user.getId())
                .build();
    }
}
