package my.web.com.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import my.web.com.model.Role;
import my.web.com.model.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.validation.constraints.*;

@Data
@NoArgsConstructor
public class RequestUserDto {

    private int id;

    @NotBlank
    @Size(min = 3, max = 15)
    private String login;

    @NotBlank
    @Size(min = 3)
    private String password;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @NotNull
    private Role role;

    @NotNull
    @Min(18)
    private int age;

    @Email
    private String email;

    private int tokens;

    public static User convertToUser(RequestUserDto userDao) {
        BCryptPasswordEncoder cryptPasswordEncoder = new BCryptPasswordEncoder();
        return User.builder()
                .age(userDao.getAge())
                .email(userDao.getEmail())
                .firstName(userDao.getFirstName())
                .lastName(userDao.getLastName())
                .login(userDao.getLogin())
                .password(cryptPasswordEncoder.encode(userDao.getPassword()))
                .tokens(userDao.getTokens())
                .role(userDao.getRole())
                .build();
    }
}

