package my.web.com.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
public class AuthenticationResponseDto {
    private ResponseUserDto user;
    private String token;
}
