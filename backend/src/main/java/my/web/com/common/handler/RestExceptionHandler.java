package my.web.com.common.handler;

import lombok.extern.log4j.Log4j2;
import my.web.com.common.exceptions.JwtAuthenticationException;
import my.web.com.model.dto.ApiError;
import my.web.com.model.dto.ValidationError;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.*;

@RestControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(AuthenticationException.class)
    @ResponseStatus(UNAUTHORIZED)
    public ApiError authenticationException(AuthenticationException ex) {
        return new ApiError(UNAUTHORIZED.value(), ex.getMessage());
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    @ResponseStatus(NOT_FOUND)
    public ApiError usernameNotFoundException(UsernameNotFoundException ex) {
        return new ApiError(NOT_FOUND.value(), ex.getMessage());
    }

    @ExceptionHandler(JwtAuthenticationException.class)
    @ResponseStatus(UNAUTHORIZED)
    public ApiError jwtAuthenticationException(JwtAuthenticationException ex) {
        return new ApiError(UNAUTHORIZED.value(), ex.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(UNPROCESSABLE_ENTITY)
    public List<ValidationError> methodArgumentNotValidException(MethodArgumentNotValidException ex) {
        return bindingResultToErrors(ex.getBindingResult());
    }

    private List<ValidationError> bindingResultToErrors(BindingResult result) {
        return result.getFieldErrors().stream()
                .map(fieldError -> new ValidationError(fieldError.getField(), fieldError.getDefaultMessage()))
                .collect(Collectors.toList());
    }
}
