package my.web.com.services;

import lombok.AllArgsConstructor;
import my.web.com.common.exceptions.JwtAuthenticationException;
import my.web.com.model.User;
import my.web.com.model.dto.AuthenticationRequestDto;
import my.web.com.model.dto.AuthenticationResponseDto;
import my.web.com.model.dto.ResponseUserDto;
import my.web.com.repository.CustomizedUserRepository;
import my.web.com.security.JwtTokenProvider;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AuthServices {
    private final AuthenticationManager authenticationManager;
    private final CustomizedUserRepository userRepository;
    private final JwtTokenProvider jwtTokenProvider;

    public AuthenticationResponseDto login(AuthenticationRequestDto request) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getLogin(),
                request.getPassword()));
        User user = userRepository.findByLogin(request.getLogin())
                .orElseThrow(() -> new UsernameNotFoundException("User doesn't exists"));
        String token = jwtTokenProvider.createToken(request.getLogin(), user.getRole().name());
        return new AuthenticationResponseDto(ResponseUserDto.convertToResponseUserDto(user), token);
    }

    public ResponseUserDto validationToken(String token) {
        if (!jwtTokenProvider.validateToken(token)) {
            throw new JwtAuthenticationException("JWT token is expired or invalid", HttpStatus.UNAUTHORIZED);
        }
        User user = userRepository.findByLogin(jwtTokenProvider.getLogin(token))
                .orElseThrow(() -> new UsernameNotFoundException("User doesn't exists"));
        return ResponseUserDto.convertToResponseUserDto(user);
    }

}
