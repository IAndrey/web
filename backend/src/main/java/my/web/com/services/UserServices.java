package my.web.com.services;

import lombok.AllArgsConstructor;
import my.web.com.model.dto.RequestUserDto;
import my.web.com.model.dto.ResponseUserDto;
import my.web.com.model.User;
import my.web.com.repository.CustomizedUserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import my.web.com.security.SecurityUser;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserServices implements UserDetailsService {

    private final CustomizedUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userRepository.findByLogin(login).orElseThrow(() ->
                new UsernameNotFoundException("User doesn't exists"));
        return new SecurityUser(user.getLogin(), user.getPassword(), user.getRole(), true);
    }

    public void saveUser(RequestUserDto userDao) {
        User user = RequestUserDto.convertToUser(userDao);
        userRepository.save(user);
    }

    public void deleteUserById(int id) {
        userRepository.deleteById(id);
    }

    public ResponseUserDto findUserByNickName(String login) {
        return ResponseUserDto.convertToResponseUserDto(userRepository.findByLogin(login).get());
    }

    public List<ResponseUserDto> findAllUsers() {
        return userRepository.findAll()
                .stream().map(ResponseUserDto::convertToResponseUserDto).collect(Collectors.toList());
    }
}
