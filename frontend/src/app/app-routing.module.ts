import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ModelComponent} from "./modules/model/model.component";
import {CategoriesComponent} from "./modules/catigories/categories.component";

const routes: Routes = [
  {
    path: '',
    component: CategoriesComponent
  },
  {
    path: 'user/:name',
    component: ModelComponent
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
