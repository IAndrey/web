import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../model/model.user";
import {Cookie} from "ng2-cookies/ng2-cookies";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly JwtToken = 'jwtToken';
  private readonly backUrl = 'http://localhost:8080'

  public user: User;

  constructor(private http: HttpClient) {
    this.validateToken();
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${this.backUrl}/user/find/all`);
  }

  public login(login: string, password: string) {
    this.http.post<any>(`${this.backUrl}/auth/login`, {
      login: login,
      password: password
    }).subscribe(response => {
        this.user = response.user;
        this.setToken(response.token);
      }, error => {
      }
    );
  }

  public validateToken() {
    const token = Cookie.get(this.JwtToken);
    if (token !== null) {
      this.http.get<User>(`${this.backUrl}/auth/validate/${token}`)
        .subscribe(response => this.user = response)
    }
  }

  public setToken(token: string) {
    Cookie.set(this.JwtToken, token);
  }

  public deleteToken() {
    Cookie.delete(this.JwtToken);
  }
}
