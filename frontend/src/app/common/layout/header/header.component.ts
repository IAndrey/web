import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UserService} from "../../services/service.user";
import {User} from "../../model/model.user";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {


  inputLogin: string;
  inputPassword: string;

  constructor(public userService: UserService) {
  }

  ngOnInit(): void {
  }

  public userLogin() {
    this.userService.login(this.inputLogin, this.inputPassword);
  }

}
