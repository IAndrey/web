import {RoleEnum} from "../enums/role.enum";

export interface User {
  id: number;
  login: string;
  password: string;
  firstName: string;
  lastName: string;
  patronymic: string;
  age: number;
  email: string;
  tokens: number;
  role: RoleEnum;
}
