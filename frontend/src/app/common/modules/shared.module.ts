import {HeaderComponent} from "../layout/header/header.component";
import {NgModule} from "@angular/core";
import {NavigationComponent} from "../../modules/view/navigation/navigation.component";
import {ModelComponent} from "../../modules/model/model.component";
import {CategoriesComponent} from "../../modules/catigories/categories.component";
import {CommonModule} from "@angular/common";
import {WebcamModule} from "ngx-webcam";
import {FooterComponent} from "../layout/footer/footer.component";
import {InfiniteScrollModule} from "ngx-infinite-scroll";
import {NgScrollbarModule} from "ngx-scrollbar";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    HeaderComponent,
    CategoriesComponent,
    NavigationComponent,
    ModelComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    WebcamModule,
    InfiniteScrollModule,
    NgScrollbarModule,
    FormsModule
  ],
  exports: [
    CategoriesComponent,
    HeaderComponent,
    NavigationComponent,
    ModelComponent,
    FooterComponent
  ],
  providers: []
})
export class SharedModule { }
