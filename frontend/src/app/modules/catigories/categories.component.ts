import {ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Subject} from "rxjs";
import {User} from "../../common/model/model.user";
import {UserService} from "../../common/services/service.user";

@Component({
  selector: 'app-catigories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  @ViewChild('webcamBlock') webcamBlock: ElementRef;

  public width;
  private widthMarker: number
  flag = false

  public numbers: number[] = [];
  public users: User[];
  private videoOptions: MediaTrackConstraints;
  private trigger: Subject<void> = new Subject<void>();
  private delimetr: number = 8;

  constructor(private cdr: ChangeDetectorRef,
              private userService: UserService){
  }

  ngOnInit(): void {
    this.initVideoOptions()
  }

  private getUsers() {
    this.userService.getUsers().subscribe( response => this.users = response);
  }

  private testCountBlock(count: number) {
    for (let i = 0; i < count; i++) {
      this.numbers[i] = i;
    }
  }

  private initVideoOptions() {
    this.videoOptions = {
      aspectRatio: 1
    }
  }

  private initWidth() {
    this.width = (this.webcamBlock.nativeElement.offsetWidth - 3) / this.delimetr;
    this.widthMarker = this.width;
  }

  public onResize(event: any) {
    // this.width = (event.target.innerWidth - 200 - 27) / this.delimetr;
    this.width = (this.webcamBlock.nativeElement.offsetWidth - 3) / this.delimetr;

    if (this.width < this.widthMarker - 20) {
      this.delimetr--;
      this.widthMarker = this.webcamBlock.nativeElement.offsetWidth / this.delimetr;
    }
  }

  // private windowListener() {
  // window.addEventListener(`resize`, event => {
  //   this.width = this.block.nativeElement.offsetWidth
  // }, false);
  // }

  // handleImage(webcamImage: WebcamImage): void {
  //   console.info('Saved webcam image', webcamImage);
  //   this.webcamImage = webcamImage;
  // }

  // public get triggerObservable(): Observable<void> {
  //   return this.trigger.asObservable();
  // }

  // getWidth() {
  //   // this.width = this.webcamBlock.nativeElement.offsetWidth / 8;
  //  }

  ngAfterViewInit() {
    // this.testCountBlock(80);
    this.getUsers()
    this.initWidth();
    this.cdr.detectChanges();
  }

}
